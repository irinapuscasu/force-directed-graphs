// set up SVG for D3
var obj = {};    //Object
obj[0] = [];    //Nodes
obj[1] = [];    //Links

var clicked = false;
const width = 600;
const height = 400;
const colors = d3.scaleOrdinal(d3.schemeCategory10);


const svg = d3.select('body')
  .append('svg')
  .attr('oncontextmenu', 'return false;')
  .attr('width', width)
  .attr('height', height);

// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.
// const nodes = [
//   { id: 0, reflexive: false },
//   { id: 1, reflexive: true },
//   { id: 2, reflexive: false }
// ];
// let lastNodeId = 2;
// const links = [
//   { source: nodes[0], target: nodes[1], left: false, right: true },
//   { source: nodes[1], target: nodes[2], left: false, right: true }
// ];

function SaveText(text, filename) {
  var a = document.createElement('a');
  a.setAttribute('href', 'data:text/plain;charset=utf-u,' + encodeURIComponent(text));
  a.setAttribute('download', filename);
  a.click()
}

// var fs = require('fs'),
// xml2js = require('xml2js');

// var parser = new xml2js.Parser();
// fs.readFile("nodes.xml",function (err,data){
// parser.parseString(data, function (err,result) {
//       console.log(JSON.stringify(result));
//       console.log('Done');
//   });
// });


// var parseString = require('xml2js').parseString;
// var xml = "<root>Hello xml2js!</root>"
// parseString(xml, function (err, result) {
//     console.log(JSON.stringify(result));
//     console.log("done");
// });

function ReadFromXml(filename) {

  // d3.xml("file.xml", function(data)
  // {console.log(data);})

  d3.xml(filename, function (data) {
    //this.obj = JSON.parse(JSON.stringify(data));
    // this.obj = data;
    console.log("xml", data);
    
    let nodesData = [].map.call(data.querySelectorAll("node"), function (nodes) {
      return {
        id: nodes.getAttribute("id"),
        reflexive: nodes.getAttribute("reflexive")==="true"
      };
    });
    var i = nodesData.length;
    var index;
    for (index = 0; index < i; index++) {
      const node1 = { id: nodesData[index].id, reflexive: nodesData[index].reflexive };
      obj[0].push(node1);
    }


    //obj[0].push(nodesData);
    let linksData = [].map.call(data.querySelectorAll("link"), function (links) {
      return {
        source: links.getAttribute("source"),
        target: links.getAttribute("target"),
        left: links.getAttribute("left")==="true",
        right: links.getAttribute("right")==="true"
      };
    });

    var j = linksData.length;
    var index;
    for (index = 0; index < j; index++) {
      const link1 = { source: linksData[index].source, target: linksData[index].target, left: linksData[index].left, right: linksData[index].right };
      obj[1].push(link1);
    }
    //obj[1].push(linksData);
     console.log("obj",obj);



    var allData = [nodesData, linksData];
    console.log( allData);
    //  //var nodes =d3.select(data).selectAll("node").nodes();//.splice(2,3);
    //  //links= d3.select(data).selectAll("link");
    //  //console.log(nodes);
    //  this.obj1=nodes;
    //  console.log("nodes",obj1);

    //   var links=nodes.slice(0,2).map(function(d) { return {source: d, target: d.nextSibling.nextSibling, left:true, right:false}; });
    //  //var links = nodes.slice(1).map(function(d) { return {source: d, target: d.parentNode}; });
    //      // console.log(nodes);
    //      // console.log(links);
    //       this.obj2=links;
    //       console.log("links",obj2);

    
    let lastNodeId = 2;
    // // init D3 force layout
    const force = d3.forceSimulation()
      .force('link', d3.forceLink().id((d) => d.id).distance(150))       //fixed distance apart
      .force('charge', d3.forceManyBody().strength(-500))                 //repeal each other
      .force('x', d3.forceX(width / 2))
      .force('y', d3.forceY(height / 2))
      .on('tick', tick);                                                  //called each time the app renders

    // // init D3 drag suppopurt
    const drag = d3.drag()
      .on('start', (d) => {
        if (!d3.event.active) force.alphaTarget(0.3).restart();         //If target is specified, sets the current
        // target alpha to the specified number in the
        // range [0,1] and returns this simulation.
        d.fx = d.x;
        d.fy = d.y;
      })
      .on('drag', (d) => {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      })
      .on('end', (d) => {
        if (!d3.event.active) force.alphaTarget(0);

        d.fx = null;
        d.fy = null;
      });

    //define arrow markers for graph links
    //for initial arrows
    svg.append('svg:defs').append('svg:marker')
      .attr('id', 'end-arrow')
      .attr('viewBox', '0 -5 10 10') //min-x, min-y, width and height
      .attr('refX', 6)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M0,-5L10,0L0,5')
      .attr('fill', '#000');

    //for added arrows 
    svg.append('svg:defs').append('svg:marker')
      .attr('id', 'start-arrow')
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 4)
      .attr('markerWidth', 3)
      .attr('markerHeight', 3)
      .attr('orient', 'auto')
      .append('svg:path')
      .attr('d', 'M10,-5L0,0L10,5')
      .attr('fill', '#000');

    // // line displayed when dragging new nodes
    // //svg::path makes the line appear while you are dragging
    const dragLine = svg.append('svg:path')
      .attr('class', 'link dragline hidden')
      .attr('d', 'M0,0L0,0'); // defines the successive coordinates of the points through which the path has to go 
    //    //M ->So, to move to (10,10) you would use the command "M 10 10"
    //    //L ->draws a line from the current position to a new position.

    // // handles to link and node element groups
    let path = svg.append('svg:g').selectAll('path');
    let circle = svg.append('svg:g').selectAll('g');   //The SVG <g> element is used to group SVG shapes together. 
    //                                                    //Once grouped you can transform the whole group of shapes as if it was a single shape.

    // // mouse event vars
    let selectedNode = null;
    let selectedLink = null;
    let mousedownLink = null;
    let mousedownNode = null;
    let mouseupNode = null;

    function resetMouseVars() {
      mousedownNode = null;
      mouseupNode = null;
      mousedownLink = null;
    }

    // update force layout (called automatically each iteration)
    function tick() {
      // draw directed edges with proper padding from node centers
      path.attr('d', (d) => {
        const deltaX = d.target.x - d.source.x;
        const deltaY = d.target.y - d.source.y;
        const dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        const normX = deltaX / dist;
        const normY = deltaY / dist;
        const sourcePadding = d.left ? 17 : 12;
        const targetPadding = d.right ? 17 : 12;
        const sourceX = d.source.x + (sourcePadding * normX);
        const sourceY = d.source.y + (sourcePadding * normY);
        const targetX = d.target.x - (targetPadding * normX);
        const targetY = d.target.y - (targetPadding * normY);

        return `M${sourceX},${sourceY}L${targetX},${targetY}`;
      });
      circle.attr('transform', (d) => `translate(${d.x},${d.y})`);
      //translates the circle on x,y; 
    }

    // update graph (called when needed)
    function restart() {
      // path (link) group
      path = path.data(allData[1]);

      // update existing links
      //?
      path.classed('selected', (d) => d === selectedLink)
        .style('marker-start', (d) => d.left ? 'url(#start-arrow)' : '')
        .style('marker-end', (d) =>d.right ? 'url(#end-arrow)' : '');

      // remove old links
      path.exit().remove();

      // add new links
      path = path.enter().append('svg:path')
        .attr('class', 'link')
        .classed('selected', (d) => d === selectedLink)
        .style('marker-start', (d) => d.left ? 'url(#start-arrow)' : '')
        // .style('marker-start', function (d){ if(d.left) return 'url(#start-arrow)';})
        .style('marker-end', (d) => d.right ? 'url(#end-arrow)' : '')
        .on('mousedown', (d) => {
          if (d3.event.ctrlKey) return;

          // select link
          mousedownLink = d;
          selectedLink = (mousedownLink === selectedLink) ? null : mousedownLink;
          selectedNode = null;
          restart();
        })
        .merge(path);


      console.log(allData[0][0].reflexive);
      // circle (node) group
      // NB: the function arg is crucial here! nodes are known by id, not by index!
      circle = circle.data(allData[0], (d) => d.id);

      // update existing nodes (reflexive & selected visual states)
      circle.selectAll('circle')
        .style('fill', (d) => (d === selectedNode) ? d3.rgb('#ff6347').brighter().toString() : '#ff6347')
        .classed('reflexive', (d) => d.reflexive);

      // remove old nodes
      circle.exit().remove();

      // add new nodes
      const g = circle.enter().append('svg:g');

      g.append('svg:circle')
        .attr('class', 'node')
        .attr('r', 12)
        .style('fill', (d) => (d === selectedNode) ? d3.rgb('#ffa500').brighter().toString() : '#ffa500')
        .style('stroke', (d) => d3.rgb('#ffa500').darker().toString())
        .classed('reflexive', (d) => d.reflexive)
        .on('mouseover', function (d) {
          if (!mousedownNode || d === mousedownNode) return;
          // enlarge target node
          d3.select(this).attr('transform', 'scale(1.1)');
        })
        .on('mouseout', function (d) {
          if (!mousedownNode || d === mousedownNode) return;
          // unenlarge target node
          d3.select(this).attr('transform', '');
        })
        .on('mousedown', (d) => {
          if (d3.event.ctrlKey) return;

          // select node
          mousedownNode = d;
          selectedNode = (mousedownNode === selectedNode) ? null : mousedownNode;
          selectedLink = null;

          // reposition drag line
          dragLine
            .style('marker-end', 'url(#end-arrow)')
            .classed('hidden', false)
            .attr('d', `M${mousedownNode.x},${mousedownNode.y}L${mousedownNode.x},${mousedownNode.y}`);

          restart();
        })
        .on('mouseup', function (d) {
          if (!mousedownNode) return;

          // needed by FF
          dragLine
            .classed('hidden', true)
            .style('marker-end', '');

          // check for drag-to-self
          mouseupNode = d;
          if (mouseupNode === mousedownNode) {
            resetMouseVars();
            return;
          }

          // unenlarge target node
          d3.select(this).attr('transform', '');

          // add link to graph (update if exists)
          // NB: links are strictly source < target; arrows separately specified by booleans
          const isRight = mousedownNode.id < mouseupNode.id;
          const source = isRight ? mousedownNode.id : mouseupNode.id;
          const target = isRight ? mouseupNode.id : mousedownNode.id;


          const link = allData[1].filter((l) => l.source === source && l.target === target)[0];
          if (link) {
            link[isRight ? 'right' : 'left'] = true;
          } else {
            console.log("%s %s", source, target);
            allData[1].push({ source, target, left: !isRight, right: isRight });
            obj[1].push({ source, target, left: !isRight, right: isRight });

          }

          // select new link
          selectedLink = link;
          selectedNode = null;
          restart();
        });

      // show node IDs
      g.append('svg:text')
        .attr('x', 0)
        .attr('y', 4)
        .attr('class', 'id')
        .text((d) => d.id);

      circle = g.merge(circle);

      // set the graph in motion
      force
        .nodes(allData[0])
        .force('link').links(allData[1]);

      force.alphaTarget(0.3).restart();
    }

    function mousedown() {
      // because :active only works in WebKit?
      svg.classed('active', true);

      if (d3.event.ctrlKey || mousedownNode || mousedownLink) return;

      // insert new node at point
      const point = d3.mouse(this);
      const node = { id: ++lastNodeId, reflexive: false, x: point[0], y: point[1] };
      const node1 = { id: lastNodeId, reflexive: false };
      //data.nodes.push(node);
      allData[0].push(node);
      obj[0].push(node1);

      restart();
    }

    function mousemove() {
      if (!mousedownNode) return;

      // update drag line
      dragLine.attr('d', `M${mousedownNode.x},${mousedownNode.y}L${d3.mouse(this)[0]},${d3.mouse(this)[1]}`);

      restart();
    }

    function mouseup() {
      if (mousedownNode) {
        // hide drag line
        dragLine
          .classed('hidden', true)
          .style('marker-end', '');
      }

      // because :active only works in WebKit?
      svg.classed('active', false);

      // clear mouse event vars
      resetMouseVars();
    }

    function spliceLinksForNode(node) {
      const toSplice = allData[1].filter((l) => l.source === node || l.target === node);
      for (const l of toSplice) {
        allData[1].splice(allData[1].indexOf(l), 1);
      }
    }

    // only respond once per keydown
    let lastKeyDown = -1;

    function keydown() {
      d3.event.preventDefault();

      if (lastKeyDown !== -1) return;
      lastKeyDown = d3.event.keyCode;

      // ctrl
      if (d3.event.keyCode === 17) {
        circle.call(drag);
        svg.classed('ctrl', true);
      }

      if (!selectedNode && !selectedLink) return;

      switch (d3.event.keyCode) {

        case 46: // delete
          if (selectedNode) {
            allData[0].splice(allData[0].indexOf(selectedNode), 1);
            spliceLinksForNode(selectedNode);
          } else if (selectedLink) {
            allData[1].splice(allData[1].indexOf(selectedLink), 1);
          }
          selectedLink = null;
          selectedNode = null;
          restart();
          break;
        case 66: // B
          if (selectedLink) {
            // set link direction to both left and right
            selectedLink.left = true;
            selectedLink.right = true;
          }
          restart();
          break;
        case 76: // L
          if (selectedLink) {
            // set link direction to left only
            selectedLink.left = true;
            selectedLink.right = false;
          }
          restart();
          break;
        case 82: // R
          if (selectedNode) {
            // toggle node reflexivity
            selectedNode.reflexive = !selectedNode.reflexive;
          } else if (selectedLink) {
            // set link direction to right only
            selectedLink.left = false;
            selectedLink.right = true;
          }
          restart();
          break;
      }
    }

    function keyup() {
      lastKeyDown = -1;

      // ctrl
      if (d3.event.keyCode === 17) {
        circle.on('.drag', null);
        svg.classed('ctrl', false);
      }
    }
    // app starts here
    svg.on('mousedown', mousedown)
      .on('mousemove', mousemove)
      .on('mouseup', mouseup);
    d3.select(window)
      .on('keydown', keydown)
      .on('keyup', keyup);
    restart();

    console.log(allData[0]);
    console.log(allData[1]);

  })

}
function print() {
  clicked = true;
  console.log(obj);
  var o2x = require('object-to-xml');
  console.log(o2x(obj));
  // SaveText(JSON.stringify(obj), "newGraph.json");
  SaveText(o2x(obj), "newGraph.xml");
}


